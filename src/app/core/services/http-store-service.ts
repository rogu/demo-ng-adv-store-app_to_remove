import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Store } from "./store";
import { share, tap, map } from 'rxjs/operators';

export function HttpStoreConfig(config: { url: string }): ClassDecorator {
  return function (constructor: any) {
    constructor.prototype.url = config.url;
  }
}

@HttpStoreConfig({ url: 'http://localhost/' })
export abstract class HttpStore<T> extends Store<T> {
  url!: string;

  constructor(state: T, private http: HttpClient) {
    super(state);
  }
  /**
   * @param  {string} extendUrl
   * @param  {HttpHeaders|null} headers
   * @param  {(resp)=>any} mapFn; get from response what you want save in store
   */
  get(extendUrl: string, headers: HttpHeaders | null = null, mapFn: (resp: any) => any = (resp) => resp) {
    const req$ = this.http.get(`${this.url}/${extendUrl}`, { ...headers && headers }).pipe(share())
    req$.pipe(
      map(mapFn),
      tap((resp: any) => this.setState(resp))
    ).subscribe();
    return req$;
  }

  post(extendUrl: string, data: any, mapFn: (resp: any) => any) {
    const req$ = this.http.post(`${this.url}/${extendUrl}`, data).pipe(share())
    req$.pipe(
      map(mapFn),
      tap((resp: any) => this.setState(resp))
    ).subscribe()
    return req$;
  }

}
