import { tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HttpStore, HttpStoreConfig } from './http-store-service';
import { Injectable } from '@angular/core';
import { AuthDataModel, HttpResponseModel } from '../../utils/models';
import { Router } from '@angular/router';
import { NotificationService } from 'src/app/shared/services/notification.service';

@HttpStoreConfig({ url: 'https://auth.debugger.pl' })
@Injectable({ providedIn: 'root' })
export class AuthHttpStoreService extends HttpStore<boolean | undefined> {
  constructor(
    private router: Router,
    http: HttpClient,
    private notification: NotificationService
  ) {
    super(undefined, http);
  }

  isLogged() {
    const authorization = localStorage.getItem('token') || '';
    if (authorization) {
      const headers = new HttpHeaders({ authorization });
      this.get('is-logged', headers, (resp: HttpResponseModel<any>) => !resp.warning);
    } else
      this.setState(false);
  }

  login(data: AuthDataModel) {
    this.post('login', data, () => true)
      .pipe(tap(({ data }: any) => {
        this.router.navigateByUrl('/items');
        localStorage.setItem('token', data.accessToken);
      }))
      .subscribe();
  }

  logout() {
    this.setState(false);
    localStorage.removeItem('token');
    this.notification.showSuccess('you are logged out');
    this.router.navigateByUrl('/login');
  }
}
