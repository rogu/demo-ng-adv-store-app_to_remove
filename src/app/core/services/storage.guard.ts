import { map, mapTo, tap } from 'rxjs/operators';
import { CartStoreService } from './../../containers/cart/services/cart.store.service';
import { CartStorageService } from './../../containers/cart/services/cart.api.ls.service';
import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { from, Observable } from 'rxjs';
import { ItemModel } from 'src/app/utils/models';
import { HttpClient } from '@angular/common/http';
import { Api } from 'src/app/utils/api';

@Injectable({
  providedIn: 'root'
})
export class StorageGuard implements CanActivate {

  constructor(
    private cartStorage: CartStorageService,
    private cartStore: CartStoreService,
    private http: HttpClient
  ) { }

  setMandatory() {
    this.http.get(Api.MANDATORY_END_POINT).subscribe(({ data }: any) => {
      data.forEach((val: ItemModel, i: number) =>
        this.cartStore.dispatch({ type: 'increase', payload: val }));
    })
  }

  canActivate(): Observable<boolean> {
    return from(this.cartStorage.get())
      .pipe(
        tap((data) => this.cartStore.setState(data || [])),
        tap((data) => !data?.length && this.setMandatory()),
        map(_ => true)
      )
  }

}
