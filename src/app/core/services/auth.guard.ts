import { Injectable } from '@angular/core';
import { CanActivateChild, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map, take, tap } from 'rxjs/operators';
import { NotificationService } from 'src/app/shared/services/notification.service';
import { AuthHttpStoreService } from './auth.store.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild {

  constructor(
    private authStore: AuthHttpStoreService,
    private notification: NotificationService,
    private router: Router
  ) { }

  canActivateChild(): Observable<boolean> {
    return this.authStore.getState()
      .pipe(
        tap((logged) => {
          if (logged === undefined) {
            this.authStore.isLogged();
          } else if (!logged) {
            this.router.createUrlTree(['login']);
            this.notification.showError('you shall not pass; try login!');
          }
        }),
        filter((val) => !!val),
        take(1),
        map(v => !!v)
      );
  }

}
