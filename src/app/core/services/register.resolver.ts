import { Api } from './../../utils/api';
import { FieldConfig } from 'demo-ng-kolekto';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterResolver implements Resolve<Observable<FieldConfig[]>> {
  constructor(private http: HttpClient) { }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<FieldConfig[]> {
    return this.http
      .get<{ data: FieldConfig[] }>(Api.DATA_FORM_CONFIG)
      .pipe(map((resp: { data: FieldConfig[] }) => resp.data));
  }
}
