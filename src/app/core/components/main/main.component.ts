import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { first, map, shareReplay } from 'rxjs/operators';
import { CartStoreService } from '../../../containers/cart/services/cart.store.service';
import { AuthHttpStoreService } from '../../services/auth.store.service';
import { WebComponentWrapperOptions } from '@angular-architects/module-federation-tools';
const { version } = require('../../../../../package.json');

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent {

  version = version;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );
  cartCount$: Observable<number> = this.cartStore.getState().pipe(map((arr) => arr.reduce((acc, item: any) => acc + item.count, 0)));;
  auth$: Observable<boolean | undefined> = this.authStore.getState();
  @ViewChild('drawer') drawer!: any;
  footer: WebComponentWrapperOptions = {
    remoteEntry: 'https://lib.debugger.pl/remoteEntry.js',
    remoteName: 'lib',
    exposedModule: './footer',
    elementName: 'ui-footer',
  }

  constructor(
    private breakpointObserver: BreakpointObserver,
    private cartStore: CartStoreService,
    private authStore: AuthHttpStoreService
  ) { }

  logOut() {
    this.authStore.logout();
  }

  navClickHandler() {
    this.isHandset$.pipe(first()).subscribe((mobile) => {
      mobile && this.drawer.toggle()
    })
  }

}
