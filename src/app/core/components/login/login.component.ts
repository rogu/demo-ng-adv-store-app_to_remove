import { ActionModel } from './../../../utils/models';
import { Component, OnInit, Optional, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthHttpStoreService } from '../../services/auth.store.service';
import { FieldConfig, FieldTypes, FormEvents, FORM_BG_COLOR } from 'demo-ng-kolekto';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formConfig: FieldConfig[] = [
    {
      label: 'username',
      name: 'username',
      value: 'admin@localhost',
      type: FieldTypes.input,
      placeholder: 'your name',
      validators: [{ name: 'required', message: 'pole wymagane' }, { name: 'email', message: 'email nie poprawny' }]
    },
    {
      label: 'password',
      name: 'password',
      value: 'Admin1',
      type: FieldTypes.password,
      placeholder: 'your password',
      validators: [{ name: 'required', message: 'pole wymagane' }]
    },
    {
      label: 'login',
      name: 'send',
      type: FieldTypes.button
    }
  ]
  auth$!: Observable<boolean | undefined>;

  constructor(
    private authHttpStore: AuthHttpStoreService,
    @Optional() @Inject(FORM_BG_COLOR) public bgColor: string
  ) { }

  ngOnInit(): void {
    this.auth$ = this.authHttpStore.getState();
  }

  onAction({ type, payload }: ActionModel<FormEvents>) {
    switch (type) {
      case FormEvents.submit:
        this.authHttpStore.login(payload);
        break;
      case FormEvents.update:
        console.log(payload);
        break;
    }
  }

}
