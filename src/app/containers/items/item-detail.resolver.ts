import { map, take, filter } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { ItemModel } from '../../utils/models';
import { ItemsStoreService } from './items.store.service';


@Injectable({
  providedIn: 'root'
})
export class ItemDetailResolver {

  constructor(private itemsStore: ItemsStoreService) { }

  resolve(route: ActivatedRouteSnapshot): Observable<ItemModel | undefined> {
    return this.itemsStore.getState()
      .pipe(
        map((arr: ItemModel[]) => arr.find((item) => item.id === route.params.id)),
        filter((val) => !!val),
        take(1)
      )
  }
}
