import { ItemsGuard } from './items.guard';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemsComponent } from './items/items.component';
import { RouterModule } from '@angular/router';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { DataGridModule } from 'demo-ng-kolekto';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ItemDetailResolver } from './item-detail.resolver';

@NgModule({
  declarations: [ItemsComponent, ItemDetailComponent],
  imports: [
    CommonModule,
    DataGridModule.forRoot({ bgColor: 'white' }),
    RouterModule.forChild([
      {
        path: '', canActivate: [ItemsGuard], component: ItemsComponent, children: [
          { path: ':id', component: ItemDetailComponent, resolve: { itemDetail: ItemDetailResolver } }
        ]
      },
    ]),
    MatIconModule,
    MatButtonModule
  ]
})
export class ItemsModule { }
