import { ItemsStoreService } from './items.store.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { tap, filter, take, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ItemsGuard implements CanActivate {
  constructor(
    private itemsStore: ItemsStoreService
  ) { }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.itemsStore.getState()
      .pipe(
        tap((val) => !val.length && this.itemsStore.fetchItems()),
        filter((val) => !!val),
        take(1),
        map(v => !!v)
      );
  }

}
