import { Component, OnInit, AfterViewInit, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { CartStoreService } from '../services/cart.store.service';
import { Observable } from 'rxjs';
import { CartItemModel } from '../../../utils/models';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartComponent implements OnInit, AfterViewInit {

  items$!: Observable<CartItemModel[]>;

  constructor(
    private cartStore: CartStoreService,
    private cd: ChangeDetectorRef
  ) { }

  ngAfterViewInit(): void {
    this.items$ = this.cartStore.getState();
    this.cd.detectChanges();
  }

  ngOnInit(): void { }

  increase(item: CartItemModel) {
    this.cartStore.dispatch({ type: 'increase', payload: item });
  }

  decrease(item: CartItemModel) {
    this.cartStore.dispatch({ type: 'decrease', payload: item });
  }

  remove(item: CartItemModel) {
    this.cartStore.dispatch({ type: 'remove', payload: item });
  }

}
