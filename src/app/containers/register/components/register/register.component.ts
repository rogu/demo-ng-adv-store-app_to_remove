import { ActivatedRoute } from '@angular/router';
import { FieldConfig, FormEvents, FormValue } from 'demo-ng-kolekto';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  formConfig$: Observable<FieldConfig[]> = this.route.data.pipe(map(({ config }) => config));
  errors!: any[] | null | undefined;

  constructor(private route: ActivatedRoute) { }

  onAction({ type, payload, errors }: FormValue) {
    this.errors = errors;
    if (type === FormEvents.submit && !errors?.length) alert(JSON.stringify(payload, null, 4));
  }
}
