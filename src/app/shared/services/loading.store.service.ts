import { Injectable } from '@angular/core';
import { Store } from '../../core/services/store';


@Injectable({
  providedIn: 'root'
})
export class LoadingStateService extends Store<boolean> {

  constructor() {
    super(false);
  }

  setState(value: boolean) {
    super.setState(value);
  }

}
