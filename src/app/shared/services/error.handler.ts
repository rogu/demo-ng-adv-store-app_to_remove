import { NotificationService } from './notification.service';
import { ErrorHandler, Injectable, NgZone } from '@angular/core';
import { HttpErrorResponse } from "@angular/common/http";
@Injectable()
export class GlobalErrorHandler implements ErrorHandler {
  constructor(
    private zone: NgZone,
    private notification: NotificationService
  ) { }

  handleError(error: any) {

    // Check if it's an error from an HTTP response
    if (!(error instanceof HttpErrorResponse)) {
      error = error.rejection; // get the error object
    }
    this.zone.run(() => {
     // alert(error?.message || 'any error')
    });

    this.notification.showError(error.message);
  }
}
