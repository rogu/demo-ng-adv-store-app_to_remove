import { LoadingStateService } from './../../services/loading.store.service';
import { switchMap, delay, map, tap } from 'rxjs/operators';
import { Component, OnInit, Input } from '@angular/core';
import { Router, RouteConfigLoadStart } from '@angular/router';
import { BehaviorSubject, iif, merge, of, Observable, concat, combineLatest } from 'rxjs';

@Component({
  selector: 'app-spinner',
  template: `
  <div class="wrapper" *ngIf="show$ | async">
    <div class="circle" >
      <mat-spinner diameter="50" color="warn"></mat-spinner>
    </div>
  </div>
  `,
  styles: [`
  .wrapper {
    position: absolute;
    width: 100%;
    height: 100vh;
    background-color: rgba(0,0,0,.6);
    left:0;
    z-index: 100;
    top:0;
    transition: background-color 200ms linear;
    display: flex;
  }
  .circle {
    margin: auto;
  }
  `]
})
export class SpinnerComponent implements OnInit {

  @Input() loading!: boolean | null;
  show$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  loading$: Observable<boolean> = this.loadingStore.getState();

  constructor(
    private router: Router,
    private loadingStore: LoadingStateService
  ) { }

  ngOnInit() {
    combineLatest(
      [
        this.loading$,
        this.router.events.pipe(map((ev) => ev instanceof RouteConfigLoadStart))
      ]
    )
      .pipe(
        switchMap((ev: any[]) => iif(() => ev.includes(true), of(true), of(false).pipe(delay(500))))
      )
      .subscribe(ev => this.show$.next(ev));
  }

}
