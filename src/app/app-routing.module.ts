import { RegisterResolver } from './core/services/register.resolver';
import { StorageGuard } from './core/services/storage.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InitComponent } from './core/components/init/init.component';
import { LoginComponent } from './core/components/login/login.component';
import { AuthGuard } from './core/services/auth.guard';

const routes: Routes = [
  {
    path: '', canActivate: [StorageGuard], children: [
      { path: '', component: InitComponent },
      { path: 'login', component: LoginComponent },
      { path: 'register', resolve:{config:RegisterResolver}, loadChildren: () => import('./containers/register/register.module').then((m) => m.RegisterModule) },
      {
        path: '', canActivateChild: [AuthGuard], children: [
          { path: 'game', loadChildren: () => import('./containers/game/game.module').then((m) => m.GameModule) },
          { path: 'items', loadChildren: () => import('./containers/items/items.module').then((m) => m.ItemsModule) },
          { path: 'cart', loadChildren: () => import('./containers/cart/cart.module').then((m) => m.CartModule) }
        ]
      },
      { path: '**', component: InitComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
