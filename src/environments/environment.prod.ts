export const environment = {
  production: true,
  gameUrl: '//demo-game.debugger.pl/',
  gameWsUrl: 'wss://demo-game.debugger.pl/',
};
